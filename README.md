# Painter's algorithm
Painter's algorithm is Java Swing application presenting painter's algorithm

## Functionalities implemented
* Presenting objects on the scene with faces 
* Moving camera around the scene with displaying faces


## Technologies used
* Java
* Swing library
* Maven
* MVP pattern