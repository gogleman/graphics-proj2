package view;

import core.Face2D;
import core.Point2D;
import model.Scene;
import model.SceneObject;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

import static view.AppFrame.FRAME_WIDTH;

public class ScenePanel extends JPanel{

    private Color COLOR = Color.WHITE;
    private Scene scene;


    public ScenePanel() {
    }

    @Override
    public void paintComponent(Graphics g){
        super.paintComponent(g);
        g.setColor(COLOR);
        drawObject(g);

    }

    private void drawObject(Graphics g){

        for (SceneObject object : scene.getObjects()) {

            //todo Dodać rysowanie ścian 2d: iterowanie po faces2d i rysowanie polygonów, poniższe rysowanie wykomentować
            /* Rysowanie krawędzi
            LinkedList<Line2D> lines2d = object.getLines2d();
            for (Line2D line2D : lines2d) {
                int x1 = line2D.getStartPoint().getX()+FRAME_WIDTH/2;
                int y1 = -(line2D.getStartPoint().getY()-FRAME_HEIGHT/2);
                int x2 = line2D.getEndPoint().getX()+FRAME_WIDTH/2;
                int y2 = -(line2D.getEndPoint().getY()-FRAME_HEIGHT/2);
                //g.drawLine(x1, y1, x2, y2);
            }
            */

            ArrayList<Face2D> faces2d = object.getFaces2d();
            for(Face2D face2D : faces2d){
                int xPoints[] = new int[4];
                int yPoints[] = new int[4];
                int pointNumber=0;
                for(Point2D point2D : face2D.getFace2dPoints()){
                    xPoints[pointNumber] = point2D.getX()+FRAME_WIDTH/2;
                    yPoints[pointNumber] = -(point2D.getY()-FRAME_WIDTH/2);
                    pointNumber++;
                }
                Polygon polygon = new Polygon(xPoints,yPoints,4);
                g.setColor(Color.CYAN);
                g.fillPolygon(polygon);
                g.setColor(Color.RED);
                g.drawPolygon(polygon);

            }
        }

        /*int xPoints[]={100,300,300,100};
        int yPoints[]={100,100,300,300};
        Polygon polygon = new Polygon(xPoints,yPoints,4);
        g.setColor(Color.CYAN);
        g.fillPolygon(polygon);
        g.setColor(Color.magenta);
        g.drawPolygon(polygon);*/
    }

    public void setScene(Scene scene) {
        this.scene = scene;
    }
}
