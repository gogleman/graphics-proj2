import model.DefaultCuboids;
import model.Scene;
import presenter.ScenePresenter;
import view.AppFrame;

public class Main {

    public static void main(String [] args){

        DefaultCuboids defaultCuboids = new DefaultCuboids();

        Scene scene = new Scene();
        scene.addObject(defaultCuboids.getCuboids().get(0));
        scene.addObject(defaultCuboids.getCuboids().get(1));
        scene.addObject(defaultCuboids.getCuboids().get(2));
        scene.addObject(defaultCuboids.getCuboids().get(3));

        ScenePresenter scenePresenter = new ScenePresenter();
        AppFrame af = new AppFrame(scenePresenter,scene);
        scenePresenter.setScene(scene);
        scenePresenter.setAppFrame(af);
        scenePresenter.setSceneOnFrame();

        af.open();

    }
}
