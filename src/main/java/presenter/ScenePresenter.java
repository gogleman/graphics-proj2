package presenter;

import algorithms.Algorithms;
import algorithms.Vectors;
import model.Scene;
import model.SceneObject;
import view.AppFrame;
import view.TransformationButtons;

import java.util.LinkedList;


public class ScenePresenter {

    private AppFrame appFrame;
    private Scene scene;

    public ScenePresenter() {
    }

    public void closeButtonClicked(){
        System.exit(0);
    }

    public void transformationButtonClicked(TransformationButtons.ButtonType buttonType){

        LinkedList<SceneObject> objects = scene.getObjects();

        switch(buttonType){
            case MOVE_LEFT:
                Algorithms.transformPoints(Vectors.TRANSLATION_LEFT,scene.getObjects());
                break;
            case MOVE_RIGHT:
                Algorithms.transformPoints(Vectors.TRANSLATION_RIGHT,objects);
                break;
            case MOVE_DOWN:
                Algorithms.transformPoints(Vectors.TRANSLATION_DOWN,objects);
                break;
            case MOVE_UP:
                Algorithms.transformPoints(Vectors.TRANSLATION_UP,objects);
                break;
            case MOVE_BACKWARD:
                Algorithms.transformPoints(Vectors.TRANSLATION_BACKWARD,objects);
                break;
            case MOVE_FORWARD:
                Algorithms.transformPoints(Vectors.TRANSLATION_FORWARD,objects);
                break;
            case ROTATE_X:
                Algorithms.transformPoints(Vectors.ROTATION_X,objects);
                break;
            case ROTATE_X_MINUS:
                Algorithms.transformPoints(Vectors.ROTATION_X_MINUS,objects);
                break;
            case ROTATE_Y:
                Algorithms.transformPoints(Vectors.ROTATION_Y,objects);
                break;
            case ROTATE_Y_MINUS:
                Algorithms.transformPoints(Vectors.ROTATION_Y_MINUS,objects);
                break;
            case ROTATE_Z:
                Algorithms.transformPoints(Vectors.ROTATION_Z,objects);
                break;
            case ROTATE_Z_MINUS:
                Algorithms.transformPoints(Vectors.ROTATION_Z_MINUS,objects);
                break;
            case ZOOM_IN:
                for(SceneObject sceneObject : objects){
                    sceneObject.setDistance(sceneObject.getDistance()+Vectors.DISTANCE_STEP);
                }
                break;
            case ZOOM_OUT:
                for(SceneObject sceneObject : objects){
                    sceneObject.setDistance(sceneObject.getDistance()-Vectors.DISTANCE_STEP);
                }
                break;
        }

        appFrame.repaint();
    }

    public void setSceneOnFrame(){
        appFrame.setScene(this.scene);
    }

    public void setAppFrame(AppFrame appFrame) {
        this.appFrame = appFrame;
    }

    public void setScene(Scene scene) {
        this.scene = scene;
    }
}


