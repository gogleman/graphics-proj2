package algorithms;

import core.Point2D;
import core.Point3D;
import model.SceneObject;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

import java.util.LinkedList;
import java.util.Map;

public class Algorithms {


    public static Point2D transform3dTo2d(Point3D point3D, int distance){

        int x = (point3D.getX() * distance) / (point3D.getZ()+distance);
        int y = (point3D.getY() * distance) / (point3D.getZ()+distance);

        return new Point2D(x,y);
    }

    public static void transformPoints(double [][] transformationArray, LinkedList<SceneObject> sceneObjects){

        for (SceneObject sceneObject : sceneObjects){
            for (Map.Entry<Integer,Point3D> entry: sceneObject.getPoints3d().entrySet()){
                Point3D initialPoint = entry.getValue();
                Point3D transformedPoint;

                RealVector point3dMatrix = transformPoint3dToMatrix(initialPoint);
                RealMatrix transformationMatrix = transformToMatrix(transformationArray);
                RealVector outputMatrix = transformationMatrix.operate(point3dMatrix);

                transformedPoint = transformToPoint3d(outputMatrix);
                entry.setValue(transformedPoint);
                sceneObject.getPoints2d().get(entry.getKey());
            }

            sceneObject.recalculatePoints();
        }
    }

    private static RealVector transformPoint3dToMatrix(Point3D point3D){
        double [] matrixData = {point3D.getX(),point3D.getY(),point3D.getZ(),1};
        return new ArrayRealVector(matrixData);
    }

    private static RealMatrix transformToMatrix(double [][] transformationArray){
        return new Array2DRowRealMatrix(transformationArray);
    }

    private static Point3D transformToPoint3d(RealVector vector){
        return new Point3D((int)vector.getEntry(0),(int)vector.getEntry(1),(int)vector.getEntry(2));
    }
}
