package algorithms;


public class Vectors {

    public static final int STEP = 10;
    public static final double ANGLE = 0.05;
    public static final int DISTANCE_STEP = 30;

    public static final double TRANSLATION_LEFT [][] = {{1 , 0 , 0 , STEP},
                                                        {0 , 1 , 0 , 0},
                                                        {0 , 0 , 1 , 0},
                                                        {0 , 0 , 0 , 1}};

    public static final double TRANSLATION_RIGHT [][] = {{1 , 0 , 0 , -STEP},
            {0 , 1 , 0 , 0},
            {0 , 0 , 1 , 0},
            {0 , 0 , 0 , 1}};

    public static final double TRANSLATION_DOWN [][] = {{1 , 0 , 0 , 0},
            {0 , 1 , 0 , STEP},
            {0 , 0 , 1 , 0},
            {0 , 0 , 0 , 1}};

    public static final double TRANSLATION_UP [][] = {{1 , 0 , 0 , 0},
            {0 , 1 , 0 , -STEP},
            {0 , 0 , 1 , 0},
            {0 , 0 , 0 , 1}};

    public static final double TRANSLATION_BACKWARD [][] = {{1 , 0 , 0 , 0},
            {0 , 1 , 0 , 0},
            {0 , 0 , 1 , STEP},
            {0 , 0 , 0 , 1}};

    public static final double TRANSLATION_FORWARD [][] = {{1 , 0 , 0 , 0},
            {0 , 1 , 0 , 0},
            {0 , 0 , 1 , -STEP},
            {0 , 0 , 0 , 1}};

    public static final double ROTATION_X [][] = {{1 , 0 , 0 , 0},
            {0 , Math.cos(ANGLE) , -Math.sin(ANGLE) , 0},
            {0 , Math.sin(ANGLE) , Math.cos(ANGLE) , 0},
            {0 , 0 , 0 , 1}};

    public static final double ROTATION_X_MINUS [][] = {{1 , 0 , 0 , 0},
            {0 , Math.cos(-ANGLE) , -Math.sin(-ANGLE) , 0},
            {0 , Math.sin(-ANGLE) , Math.cos(-ANGLE) , 0},
            {0 , 0 , 0 , 1}};

    public static final double ROTATION_Y [][] = {{Math.cos(ANGLE) , 0 , Math.sin(ANGLE) , 0},
            {0 , 1 , 0 , 0},
            {-Math.sin(ANGLE) , 0 , Math.cos(ANGLE) , 0},
            {0 , 0 , 0 , 1}};

    public static final double ROTATION_Y_MINUS [][] = {{Math.cos(-ANGLE) , 0 , Math.sin(-ANGLE) , 0},
            {0 , 1 , 0 , 0},
            {-Math.sin(-ANGLE) , 0 , Math.cos(-ANGLE) , 0},
            {0 , 0 , 0 , 1}};

    public static final double ROTATION_Z [][] = {{Math.cos(ANGLE) , -Math.sin(ANGLE) , 0 , 0},
            {Math.sin(ANGLE) , Math.cos(ANGLE) , 0 , 0},
            {0 , 0 , 1 , 0},
            {0 , 0 , 0 , 1}};

    public static final double ROTATION_Z_MINUS [][] = {{Math.cos(-ANGLE) , -Math.sin(-ANGLE) , 0 , 0},
            {Math.sin(-ANGLE) , Math.cos(-ANGLE) , 0 , 0},
            {0 , 0 , 1 , 0},
            {0 , 0 , 0 , 1}};


}
