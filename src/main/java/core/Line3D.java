package core;

public class Line3D {
    private Point3D startPoint;
    private Point3D endPoint;

    public Line3D(Point3D startPoint, Point3D endPoint){
        this.startPoint = startPoint;
        this.endPoint = endPoint;
    }

    @Override
    public String toString() {
        return "Line3D{" +
                "startPoint=" + startPoint +
                ", endPoint=" + endPoint +
                '}';
    }
}
