package core;

import java.util.LinkedList;
import java.util.List;

public class Face3D{

   private List<Point3D> face3dPoints;
   private Point3D centroid;

    public Face3D(){
        face3dPoints = new LinkedList<>();
    }

    public Face3D(Point3D p1, Point3D p2, Point3D p3, Point3D p4){
        face3dPoints = new LinkedList<>();
        face3dPoints.add(p1);
        face3dPoints.add(p2);
        face3dPoints.add(p3);
        face3dPoints.add(p4);
        centroid = countCentroid();
    }

    private Point3D countCentroid(){
        int centroidX = (face3dPoints.get(0).getX()+face3dPoints.get(1).getX()+face3dPoints.get(2).getX()+face3dPoints.get(3).getX())/4;
        int centroidY = (face3dPoints.get(0).getY()+face3dPoints.get(1).getY()+face3dPoints.get(2).getY()+face3dPoints.get(3).getY())/4;
        int centroidZ = (face3dPoints.get(0).getZ()+face3dPoints.get(1).getZ()+face3dPoints.get(2).getZ()+face3dPoints.get(3).getZ())/4;
        return new Point3D(centroidX,centroidY,centroidZ);
    }

    public List<Point3D> getFace3dPoints() {
        return face3dPoints;
    }

    public Point3D getCentroid() {
        return centroid;
    }


    @Override
    public String toString() {
        return "Face3D{" +
                "face3dPoints=" + face3dPoints +
                '}';
    }

}
