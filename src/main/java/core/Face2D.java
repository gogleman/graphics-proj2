package core;

import java.util.LinkedList;
import java.util.List;

public class Face2D {
    private List<Point2D> face2dPoints;

    public Face2D(){
        face2dPoints = new LinkedList<>();
    }

    public Face2D(Point2D p1, Point2D p2, Point2D p3, Point2D p4){
        face2dPoints = new LinkedList<>();
        face2dPoints.add(p1);
        face2dPoints.add(p2);
        face2dPoints.add(p3);
        face2dPoints.add(p4);
    }

    public void addPoint(Point2D p2D){
        face2dPoints.add(p2D);
    }

    public List<Point2D> getFace2dPoints() {
        return face2dPoints;
    }

    @Override
    public String toString() {
        return "Face2D{" +
                "face2dPoints=" + face2dPoints +
                '}';
    }
}
