package model;

import core.Point3D;

import java.util.HashMap;

public class DefaultCuboids {

    private HashMap<Integer, Cuboid> cuboids;
    private static HashMap<Integer,Point3D> cuboidPoints3d;

    public DefaultCuboids(){
        cuboids = new HashMap<Integer, Cuboid>();
        createCuboid1();
        createCuboid2();
        createCuboid3();
        createCuboid4();

    }

    private void createCuboid1(){
        cuboidPoints3d = new HashMap<Integer, Point3D>();

        cuboidPoints3d.put(0,new Point3D(400,100,500));
        cuboidPoints3d.put(1,new Point3D(600,100,500));
        cuboidPoints3d.put(2,new Point3D(600,100,700));
        cuboidPoints3d.put(3,new Point3D(400,100,700));
        cuboidPoints3d.put(4,new Point3D(400,600,500));
        cuboidPoints3d.put(5,new Point3D(600,600,500));
        cuboidPoints3d.put(6,new Point3D(600,600,700));
        cuboidPoints3d.put(7,new Point3D(400,600,700));

        Cuboid cuboid = new Cuboid(cuboidPoints3d);
        cuboids.put(0, cuboid);
    }

    private void createCuboid2(){
        cuboidPoints3d = new HashMap<Integer, Point3D>();

        cuboidPoints3d.put(0,new Point3D(-400,100,500));
        cuboidPoints3d.put(1,new Point3D(-600,100,500));
        cuboidPoints3d.put(2,new Point3D(-600,100,700));
        cuboidPoints3d.put(3,new Point3D(-400,100,700));
        cuboidPoints3d.put(4,new Point3D(-400,600,500));
        cuboidPoints3d.put(5,new Point3D(-600,600,500));
        cuboidPoints3d.put(6,new Point3D(-600,600,700));
        cuboidPoints3d.put(7,new Point3D(-400,600,700));

        Cuboid cuboid = new Cuboid(cuboidPoints3d);
        cuboids.put(1, cuboid);
    }

    private void createCuboid3(){
        cuboidPoints3d = new HashMap<Integer, Point3D>();

        cuboidPoints3d.put(0,new Point3D(-400,100,1400));
        cuboidPoints3d.put(1,new Point3D(-600,100,1400));
        cuboidPoints3d.put(2,new Point3D(-600,100,1600));
        cuboidPoints3d.put(3,new Point3D(-400,100,1600));
        cuboidPoints3d.put(4,new Point3D(-400,600,1400));
        cuboidPoints3d.put(5,new Point3D(-600,600,1400));
        cuboidPoints3d.put(6,new Point3D(-600,600,1600));
        cuboidPoints3d.put(7,new Point3D(-400,600,1600));

        Cuboid cuboid = new Cuboid(cuboidPoints3d);
        cuboids.put(2, cuboid);
    }

    private void createCuboid4(){
        cuboidPoints3d = new HashMap<Integer, Point3D>();

        cuboidPoints3d.put(0,new Point3D(400,100,1400));
        cuboidPoints3d.put(1,new Point3D(600,100,1400));
        cuboidPoints3d.put(2,new Point3D(600,100,1600));
        cuboidPoints3d.put(3,new Point3D(400,100,1600));
        cuboidPoints3d.put(4,new Point3D(400,600,1400));
        cuboidPoints3d.put(5,new Point3D(600,600,1400));
        cuboidPoints3d.put(6,new Point3D(600,600,1600));
        cuboidPoints3d.put(7,new Point3D(400,600,1600));

        Cuboid cuboid = new Cuboid(cuboidPoints3d);
        cuboids.put(3, cuboid);
    }



    public HashMap<Integer, Cuboid> getCuboids() {
        return cuboids;
    }

    public HashMap<Integer, Point3D> getCuboid1Points3d() {
        return cuboidPoints3d;
    }
}
