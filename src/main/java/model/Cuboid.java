package model;

import algorithms.Algorithms;
import core.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class Cuboid implements SceneObject{

    private HashMap<Integer, Point3D> points3d;
    private LinkedList<Line3D> lines3d;
    private HashMap<Integer, Point2D> points2d;
    private LinkedList<Line2D> lines2d;
    private ArrayList<Face3D> faces3d;
    private ArrayList<Face2D> faces2d;
    //private Face3D [] faces3d;
    private static final int INITIAL_DISTANCE = 1560;
    private int distance = INITIAL_DISTANCE;

    private Cuboid(){

        points3d = new HashMap<>();
        lines3d = new LinkedList<>();
        points2d = new HashMap<>();
        lines2d = new LinkedList<>();
        faces3d = new ArrayList<>();
        faces2d = new ArrayList<>();
    }

    public Cuboid(HashMap<Integer, Point3D> points3d){
        this();
        this.points3d = points3d;
        create3dLines();
        create2dPoints();
        create2dLines();
        createFaces3d();
        sortFaces3d();
        createFaces2d();

    }

    private void create3dLines(){
        lines3d.add(new Line3D(points3d.get(0),points3d.get(1)));
        lines3d.add(new Line3D(points3d.get(1),points3d.get(2)));
        lines3d.add(new Line3D(points3d.get(2),points3d.get(3)));
        lines3d.add(new Line3D(points3d.get(3),points3d.get(0)));
        lines3d.add(new Line3D(points3d.get(0),points3d.get(4)));
        lines3d.add(new Line3D(points3d.get(1),points3d.get(5)));
        lines3d.add(new Line3D(points3d.get(2),points3d.get(6)));
        lines3d.add(new Line3D(points3d.get(3),points3d.get(7)));
        lines3d.add(new Line3D(points3d.get(4),points3d.get(5)));
        lines3d.add(new Line3D(points3d.get(5),points3d.get(6)));
        lines3d.add(new Line3D(points3d.get(6),points3d.get(7)));
        lines3d.add(new Line3D(points3d.get(7),points3d.get(4)));
    }

    private void create2dPoints(){
        for(Map.Entry<Integer,Point3D> entry : points3d.entrySet()){
            int id = entry.getKey();
            Point3D point3D = entry.getValue();
            points2d.put(id, Algorithms.transform3dTo2d(point3D,distance));
        }
    }

    private void create2dLines(){

        lines2d.add(new Line2D(points2d.get(0),points2d.get(1)));
        lines2d.add(new Line2D(points2d.get(1),points2d.get(2)));
        lines2d.add(new Line2D(points2d.get(2),points2d.get(3)));
        lines2d.add(new Line2D(points2d.get(3),points2d.get(0)));
        lines2d.add(new Line2D(points2d.get(0),points2d.get(4)));
        lines2d.add(new Line2D(points2d.get(1),points2d.get(5)));
        lines2d.add(new Line2D(points2d.get(2),points2d.get(6)));
        lines2d.add(new Line2D(points2d.get(3),points2d.get(7)));
        lines2d.add(new Line2D(points2d.get(4),points2d.get(5)));
        lines2d.add(new Line2D(points2d.get(5),points2d.get(6)));
        lines2d.add(new Line2D(points2d.get(6),points2d.get(7)));
        lines2d.add(new Line2D(points2d.get(7),points2d.get(4)));
    }

    public void recalculatePoints (){
        this.points2d = new HashMap<>();
        create2dPoints();
        this.lines3d = new LinkedList<>();
        create3dLines();
        this.lines2d = new LinkedList<>();
        create2dLines();
        this.faces3d = new ArrayList<>();
        createFaces3d();
        sortFaces3d();
        this.faces2d = new ArrayList<>();
        createFaces2d();
    }

    public HashMap<Integer, Point3D> getPoints3d() {
        return points3d;
    }

    public LinkedList<Line3D> getLines3d() {
        return lines3d;
    }

    public HashMap<Integer, Point2D> getPoints2d() {
        return points2d;
    }

    public LinkedList<Line2D> getLines2d() {
        return lines2d;
    }

    public void setDistance(int distance){
        this.distance = distance;
        recalculatePoints();
    }

    private void createFaces3d(){
        faces3d.add(0, new Face3D(points3d.get(0),points3d.get(1),points3d.get(2),points3d.get(3)));
        faces3d.add(1, new Face3D(points3d.get(4),points3d.get(5),points3d.get(6),points3d.get(7)));
        faces3d.add(2, new Face3D(points3d.get(0),points3d.get(1),points3d.get(5),points3d.get(4)));
        faces3d.add(3, new Face3D(points3d.get(1),points3d.get(2),points3d.get(6),points3d.get(5)));
        faces3d.add(4, new Face3D(points3d.get(2),points3d.get(3),points3d.get(7),points3d.get(6)));
        faces3d.add(5, new Face3D(points3d.get(3),points3d.get(0),points3d.get(4),points3d.get(7)));
    }

    public ArrayList<Face3D> getFaces3d() {
        return faces3d;
    }

    private void sortFaces3d(){
        faces3d.sort((a,b)->b.getCentroid().compareTo(a.getCentroid()));
    }

    private void createFaces2d(){
        //todo dodać tworzenie listy ścian 2d tak jak tworzenie listy punktów 2d
        for(Face3D face3d : faces3d){
            Face2D f2d = new Face2D();
            for(Point3D point3D : face3d.getFace3dPoints()){
                f2d.addPoint(Algorithms.transform3dTo2d(point3D,distance));
            }
            faces2d.add(f2d);
        }
    }

    public ArrayList<Face2D> getFaces2d() {
        return faces2d;
    }

    public int getDistance() {
        return distance;
    }

    @Override
    public String toString() {
        return "Cuboid{" +
                "points3d=" + points3d +
                ", points2d=" + points2d +
                '}';
    }
}