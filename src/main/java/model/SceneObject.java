package model;

import core.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

public interface SceneObject {

    HashMap<Integer, Point3D> getPoints3d();
    LinkedList<Line3D> getLines3d();
    HashMap<Integer, Point2D> getPoints2d();
    LinkedList<Line2D> getLines2d();
    void recalculatePoints();
    void setDistance(int distance);
    int getDistance();
    ArrayList<Face3D> getFaces3d();
    ArrayList<Face2D> getFaces2d();
}
