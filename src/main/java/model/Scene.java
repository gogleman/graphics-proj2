package model;

import core.Line2D;

import java.util.LinkedList;

/**
 * Groups objects on the scene, e.g cuboids
 */
public class Scene {

    private LinkedList<SceneObject> objects;

    public Scene() {
        objects = new LinkedList<SceneObject>();
    }

    public Scene(LinkedList<SceneObject> objects) {
        this.objects = objects;
    }

    public void addObject(SceneObject sceneObject){
        this.objects.add(sceneObject);
    }

    public LinkedList<SceneObject> getObjects() {
        return objects;
    }

    @Override
    public String toString() {

        StringBuilder stringBuilder = new StringBuilder();
        int x1,y1,x2,y2;

        for (SceneObject object : this.getObjects()) {
            stringBuilder.append("\nObject =\n");
            LinkedList<Line2D> lines2d = object.getLines2d();
            for (Line2D line2D : lines2d) {
                x1 = line2D.getStartPoint().getX();
                y1 = line2D.getStartPoint().getY();
                x2 = line2D.getEndPoint().getX();
                y2 = line2D.getEndPoint().getY();
                stringBuilder.append("x1:").append(x1).append(", y1:").append(y1);
                stringBuilder.append(", x2:").append(x2).append(", y2:").append(y2).append("\n");
            }
        }
        return "Scene{" +
                stringBuilder +
                '}';
    }
}
