import core.Line2D;
import core.Line3D;
import model.Cuboid;
import model.DefaultCuboids;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;

public class CuboidTest {

    private Cuboid cuboid;

    @Before
    public void prepare(){
        DefaultCuboids defaultCuboids = new DefaultCuboids();
        cuboid = defaultCuboids.getCuboids().get(0);
    }

    @Test
    public void showPoints3d(){
        for(Map.Entry entry : cuboid.getPoints3d().entrySet()){
            System.out.println(entry);

        }
    }

    @Test
    public void showLines3d(){
        for(Line3D line3D : cuboid.getLines3d()){
            System.out.println(line3D);
        }
    }

    @Test
    public void showPoints2d(){
        for (Map.Entry entry : cuboid.getPoints2d().entrySet()){
            System.out.println(entry);
        }
    }

    @Test
    public void showLines2d(){
        for(Line2D line2D : cuboid.getLines2d()){
            System.out.println(line2D);
        }
    }
}
